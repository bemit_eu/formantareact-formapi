import React from "react";
import styled, {keyframes} from "styled-components";
import withNamespaces from "../system/withNamespaces";

import Loading from "./Loading";

//
// Form System Helper

class FormUniqueStore {
    static keys = [];

    static add(key) {
        if(-1 === FormUniqueStore.keys.indexOf(key)) {
            // key isn't existing till now
            FormUniqueStore.keys.push(key);
            return true;
        }

        return false;
    }

    static create() {
        let key = Math.floor((1 + Math.random()) * 0x1000000).toString(16).substring(1);
        if(FormUniqueStore.add(key)) {
            // when the key was an unique return it
            return key;
        }

        // or create new ones until unique
        return FormUniqueStore.create();
    }
}

export var formUnique = FormUniqueStore.create;

//
// Labels

export var Label = styled.label`
  ${props => props.required && 'text-decoration: underline;'}
`;


//
// Button

const button__hover = (props) => {
    if(props && props.theme) {
        if(props.theme.dark) {
            return 'background-color: rgba(255,255,255, 0.1);';
        } else {
            return 'background-color: rgba(0,0,0, 0.1);';
        }
    }
};

const button__active = (props) => {
    if(props && props.theme) {
        if(props.theme.dark) {
            return 'background-color: rgba(0,0,0, 0.1);';
        } else {
            return 'background-color: rgba(0,0,0, 0.05);';
        }
    }
};

export var Button = styled.button`
  padding: ${props => props.theme.input_gutter};
  border: 1px solid ${props => (props.theme.dark ? props.theme.lightGrey : props.theme.black)};
  color: ${props => (props.theme.dark ? props.theme.lightGrey : props.theme.black)};
  background: transparent;
  cursor: pointer;
  transition: 0.4s linear background-color, 0.4s linear border-color, 0.4s linear color;
  &:hover{
    ${button__hover}
  }
  &:active{
    transition: 0.15s linear background-color, 0.4s linear border-color, 0.4s linear color;
    ${button__active}
  }
`;

// todo: add working theme, somehow props.theme.dark doesnt exist in keyframes
const buttonLoadingBoarder = keyframes`
  0% {
    background-color: #2a2a2a;
  }

  60% {
    background-color: #4e4e4e;
  }
  
  100% {
    background-color: #2a2a2a;
  }
`;

export var ButtonInteractive = styled(Button)`
  &.loading {
    animation: ${buttonLoadingBoarder} 1.2s ease-in-out infinite;
  }
  &.error {
    border-color: ${props => (props.theme.color.error)};
  }
  &.success {
    border-color: ${props => (props.theme.color.success)};
  }
`;

//
// Input

export var InputText = styled.input`
  padding: ${props => props.theme.input_gutter};
  border: 1px solid ${props => (props.theme.dark ? props.theme.lightGrey : props.theme.black)};
  color: ${props => (props.theme.dark ? props.theme.lightGrey : props.theme.black)};
  background: transparent;
  transition: 0.4s linear background-color, 0.4s linear border-color, 0.4s linear color;
  &:hover{
    
  }
  &:active{
    
  }
`;

export var InputTextarea = styled.textarea`
  padding: ${props => props.theme.input_gutter};
  border: 1px solid ${props => (props.theme.dark ? props.theme.lightGrey : props.theme.black)};
  color: ${props => (props.theme.dark ? props.theme.lightGrey : props.theme.black)};
  background: transparent;
  transition: 0.4s linear background-color, 0.4s linear border-color, 0.4s linear color;
  width: 100%;
  &:hover{
    
  }
  &:active{
    
  }
`;


//
// Input Group

export var InputGroup = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-bottom: 24px;
  label {
    width: 100%;
  }
  input {
    width: 100%;
  }
`;

//
// Universal Labeling Utils

export var ReqText = withNamespaces(['common'])((props) => (
    <p><u>{props.t('common:form-end--req')}</u>, <span>{props.t('common:form-end--opt')}</span></p>
));

const PrefixStyled = styled.div`
  display: block;
`;

const SuffixStyled = styled.div`
  display: block;
`;

const LoaderReceive = withNamespaces(['common'])((props) => {
    const {t} = props;
    return (
        <PrefixStyled>
            <Loading/>
            <p>{
                'receiving' === props.process ?
                    t('common:bound-form--receiving')
                    :
                    'receiving+error' === props.process ?
                        t('common:bound-form--receiving+error')
                        : /* where `process` is false */ t('common:bound-form--initialized')
            }</p>
        </PrefixStyled>
    );
});

const LoaderIdle = withNamespaces(['common'])((props) => {
    const {t} = props;
    return (
        <SuffixStyled>
            <p>{
                'received' === props.process ?
                    t('common:bound-form--received')
                    :
                    'sended' === props.process ?
                        t('common:bound-form--sended')
                        : null
            }</p>
        </SuffixStyled>
    );
});

const LoaderSend = withNamespaces(['common'])((props) => {
    const {t} = props;
    return (
        <SuffixStyled>
            {'sending' === props.process && 'sending+error' !== props.process ?
                <Loading/> : null
            }
            <p>{
                'sending' === props.process ?
                    t('common:bound-form--sending')
                    :
                    'sending+error' === props.process ?
                        t('common:bound-form--sending+error')
                        : null
            }</p>
        </SuffixStyled>
    );
});

const FormLoadingBoundary = (props) => {
    return (
        false === props.process || 'receiving' === props.process || 'receiving+error' === props.process ?
            /*
                show loading and message and
                show no form, on:
                    initialized (false)
                    receiving
                    receiving+error
             */
            <LoaderReceive/>
            :
            ('received' === props.process || 'sending' === props.process || 'sending+error' === props.process || 'sended' === props.process) ?
                /*
                show message, on:
                    received
                    sended
                    sending
                    sending+error
                show no loading, on:
                    received
                    sended
                 */
                <React.Fragment>
                    {props.children}
                    <LoaderIdle/>
                    <LoaderSend/>
                </React.Fragment>
                : null
    );
};

export {FormLoadingBoundary, LoaderSend, LoaderReceive, LoaderIdle};
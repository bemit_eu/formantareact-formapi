# Styled Form Components and Form Labeling

## Utils

- `formUnique()` unique identifier for a form, returns everytime a unique id with 8 chars 
    

## Components

Dumb styled-components for input fields and grouped fields

- InputGroup 
- InputText
- InputTextarea 
- Label
- Button

## Universal Labeling Components

- ReqText
- FormLoadingBoundary
- LoaderSend
- LoaderReceive
- LoaderIdle 
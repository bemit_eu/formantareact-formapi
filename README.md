# FormantaReact FormApi - a Two-Way Data Binding Form

Write easy forms which receive data from an API and are pushing data to an API.

Can be only sending data (Single-Bound Form) or receiving and sending (Two-Way Bound Form). A from must not have a receiver but not a sender.

## Usage

This component provides the easy usage of forms which are fed data from an api and pushes all data of one form also to an api.

To accomplish this, it uses multiple React patterns.

A component in which the form will be used (your component).

A `receiver` and a `sender`, functions which will be used by the HOC to perform needed api queries.

The HOC `bindForm` must get your component and if you want to use external api connectors also `receiver`, `sender` and `fields`. It stores the individual states of all bound input fields and will perform needed lifecycle around them and with the api.

It will inject different props into your component which are render props, these components are dynamically created from the HOC and bound to the needed api connectors.

Your component will only be rendered after the `receiver` is finished, the HOC shows a loader and message for different states of its `process`.

Define allowed fields, so the DB to input binding will match only those when **receiving**. On submit all data is pushed to your `sender`.

Relies on `i18next` for labeling the process state, uses the namespace `common`.

> Errors of api connectors need to be captured in a higher level

> Currently only fields which value is a valid typeof string are given the `sender` back to the Api e.g. type number is a string but must be

## LifeCycle HOC

The HOC LifeCycle is tracked in the `process` state :
1. `false` when just created | dumb rendering
2. `receiving` when loading data | starting your `receiver`
2. `receiving+error` when loading data
3. `received` when data loaded - is persistent until submitting | your component exists now
4. `sending` when data is submitted
4. `sending+error` when data is submitted
5. `sended` when data is submitted

## Props Injected

These props are injected into your wrapped component:

- your added props form where your component is included
- `connect` to add sender, receiver and fields from within your component, see examples with internals
- `process` info about current lifecycle of HOC
- uses styled components from `./component/Form.js`
- `FormApi` the wrapper and actual form, with your `sender` attached to `onSubmit`
- `InputGroupText` render prop containing a group with label and a text input field which is bound to the Api
    - accepts props:
        - `name` name of the input field, must match the name of the api field to be correctly bound
        - `type` defaults to `text`, change to e.g. `email`, all valid input types
        - `lbl` a string for the innerHTML of the label
        - `required` 
    - label and input have calculated `htmlFor` and `id` props
    - `required` is added to the group, label and input
- `InputGroupTextarea`, same as Text but not a input field instead `<textarea/>`
- `resetForm` clean all inputs, function for assigning an empty string to all input fields
- `bindField` takes a `name` as param and returns all needed props for your input, just spread it in your input like: `<input name="email" {...this.props.bindField("email")/>`, returns `value` and `onChange` for input and handles their usage


## Example

- Single-Bound Form with **internal** sender
- Two-Way-Bound Form with **external** receiver, sender and fields
- Two-Way-Bound Form with **internal** receiver, sender and fields

### Single-Bound Form with Internals

```jsx harmony
import React from 'react';
// your custom api handler
import {ApiHandler, GET, POST} from '../lib/YourApiHandler';
// i18next namespace support for react/nextjs
import withNamespaces from "../system/withNamespaces";
// HOC for your commponent
import {bindForm} from "./FormApi";

class OwnForm extends React.Component {

    // possible to use state of wrapper now
    state = {
        send: false
    };
    
    constructor(props) {
        super(props);

        if(props.connect) {
            // binding internal sender through `connect`
            props.connect.sender(this.sender);
        } else {
            throw new Error('connect not found in OwnForm');
        }
    }

    // sends data on form submition
    sender = (evt, data, touched, props) => {
        // modify submit values before sending to server

        return (new ApiHandler(POST, 'cars/<some-id>', data))
            .then((res) => {
                if(res && res.data) {
                    // modify response before setting back the value to form inputs
                }
                this.setState({send: true});
                this.props.resetForm();    
                return res;
            });
    };

    render() {
        // t = frm i18next
        // FormApi = <form> element with enabled data binding
        // InputGroupText = <div> with <label> and <input> as childs, with enabled data binding on input
        const {t, FormApi, InputGroupText, bindField} = this.props;

        // get the current process to apply modifier based style rules
        const className = this.props.process + ' ' + (this.props.className || '');
        return (
            <FormApi className={className}>
                <InputGroupText name='username' lbl={t('forms:lbl-username')} required/>
                <InputGroupText name='name' lbl={t('forms:lbl-name')}/>
                <InputGroupText name='email' type='email' lbl={t('forms:lbl-email')} required/>
                
                {/* example how to add  */}
                <input type="custom" name="someval" {...this.props.bindField('someval')}/>
                
                <button>{t('forms:lbl-submit')}</button>
            </FormApi>
        );
    }
}

// injecting i18next props
// then wrapping `OwnForm` and thous injecting data bound components
export default withNamespaces(['forms'])(bindForm(OwnForm));
```

### Two-Way-Bound Form with Externals

```jsx harmony
import React from 'react';
// your custom api handler
import {ApiHandler, GET, POST} from '../lib/YourApiHandler';
// i18next namespace support for react/nextjs
import withNamespaces from "../system/withNamespaces";
// HOC for your commponent
import {bindForm} from "./FormApi";

// fields possible to match during receiving (not sending)
const fields = [
    'username',
    'name',
    'email',
];

// get data to be populated in the fields
const receiver = (props) => {
    return (new ApiHandler(GET, 'cars/<some-id>'))
        .then((res) => {
            if(res && res.data) {
                // modify response before passing to form
            }
            return res;
        });
};

// sends data on form submition
const sender = (evt, data, touched, props) => {
    // modify submit values before sending to server

    return (new ApiHandler(POST, 'cars/<some-id>', data))
        .then((res) => {
            if(res && res.data) {
                // modify response before setting back the value to form inputs
            }

            return res;
        });
};

class OwnForm extends React.Component {

    render() {
        // t = frm i18next
        // FormApi = <form> element with enabled data binding
        // InputGroupText = <div> with <label> and <input> as childs, with enabled data binding on input
        const {t, FormApi, InputGroupText, bindField} = this.props;

        // get the current process to apply modifier based style rules
        const className = this.props.process + ' ' + (this.props.className || '');
        return (
            <FormApi className={className}>
                <InputGroupText name='username' lbl={t('forms:lbl-username')} required/>
                <InputGroupText name='name' lbl={t('forms:lbl-name')}/>
                <InputGroupText name='email' type='email' lbl={t('forms:lbl-email')} required/>
                
                {/* example how to add  */}
                <input type="custom" name="someval" {...this.props.bindField('someval')}/>
                
                <button>{t('forms:lbl-submit')}</button>
            </FormApi>
        );
    }
}

// injecting i18next props
// then wrapping `OwnForm` and thous injecting data bound components
export default withNamespaces(['forms'])(bindForm(OwnForm, sender, receiver, fields));
```

### Two-Way-Bound Form with Internals

```jsx harmony
import React from 'react';
// your custom api handler
import {ApiHandler, GET, POST} from '../lib/YourApiHandler';
// i18next namespace support for react/nextjs
import withNamespaces from "../system/withNamespaces";
// HOC for your commponent
import {bindForm} from "./FormApi";

class OwnForm extends React.Component {
    
    // fields possible to match during receiving (not sending)
    fields = [
        'username',
        'name',
        'email',
    ];
    
    constructor(props) {
        super(props);

        if(props.connect) {
            props.connect.receiver(this.receiver);
            props.connect.sender(this.sender);
            props.connect.fields(this.fields);
        } else {
            throw new Error('connect not found in MessageForm');
        }
    }
    
    // get data to be populated in the fields
    receiver = (props) => {
        return (new ApiHandler(GET, 'cars/<some-id>'))
            .then((res) => {
                if(res && res.data) {
                    // modify response before passing to form
                }
                return res;
            });
    };
    
    // sends data on form submition
    sender = (evt, data, touched, props) => {
        // modify submit values before sending to server
    
        return (new ApiHandler(POST, 'cars/<some-id>', data))
            .then((res) => {
                if(res && res.data) {
                    // modify response before setting back the value to form inputs
                }
    
                return res;
            });
    };

    render() {
        // t = frm i18next
        // FormApi = <form> element with enabled data binding
        // InputGroupText = <div> with <label> and <input> as childs, with enabled data binding on input
        const {t, FormApi, InputGroupText, bindField} = this.props;

        // get the current process to apply modifier based style rules
        const className = this.props.process + ' ' + (this.props.className || '');
        return (
            /* can control granular what to show before your receiver has been finished */
            this.props.process ?
                <FormApi className={className}>
                    <InputGroupText name='username' lbl={t('forms:lbl-username')} required/>
                    <InputGroupText name='name' lbl={t('forms:lbl-name')}/>
                    <InputGroupText name='email' type='email' lbl={t('forms:lbl-email')} required/>
                    
                    {/* example how to add  */}
                    <input type="custom" name="someval" {...this.props.bindField('someval')}/>
                    
                    <button>{t('forms:lbl-submit')}</button>
                </FormApi>
                : null
        );
    }
}

// injecting i18next props
// then wrapping `OwnForm` and thous injecting data bound components
export default withNamespaces(['forms'])(bindForm(OwnForm));
```

## Licence

This project is free software distributed under the terms of two licences, the CeCILL-C and the GNU Lesser General Public License. You can use, modify and/ or redistribute the software under the terms of CeCILL-C (v1) for Europe or GNU LGPL (v3) for the rest of the world.

This file and the LICENCE.* files need to be distributed and not changed when distributing.
For more informations on the licences which are applied read: [LICENCE.md](LICENCE.md)


# Copyright

    2018 | bemit UG (haftungsbeschränkt) - project@bemit.codes
    Author: Michael Becker - michael@bemit.codes